package loader

import (
	"os"
	"path"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func prepareTmpFolder() {
	var err error

	tmpFolder := viper.GetString("tmpFolder")
	_, err = os.Stat(tmpFolder)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(tmpFolder, os.ModePerm); err != nil {
			logrus.Fatal(err)
			panic(err)
		}
	}

	preFolder := path.Join(viper.GetString("tmpFolder"), viper.GetString("img.preview.tmpFolder"))
	_, err = os.Stat(preFolder)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(preFolder, os.ModePerm); err != nil {
			logrus.Fatal(err)
			panic(err)
		}
	}

	stdFolder := path.Join(viper.GetString("tmpFolder"), viper.GetString("img.standard.tmpFolder"))
	_, err = os.Stat(stdFolder)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(stdFolder, os.ModePerm); err != nil {
			logrus.Fatal(err)
			panic(err)
		}
	}
}
