// Package loader
package loader

import (
	"memequeryer/handler"
	"path"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var configFile string = "conf.json"

// loadConfig - Loading config file by viper
func loadConfig() {
	switch gin.Mode() {
	case gin.DebugMode:
		configFile = path.Join("config", "confDebug.json")
		// for develop
		viper.WatchConfig() // <- when changing, config reload
	case gin.ReleaseMode:
		configFile = path.Join("config", "confRelease.json")
	}
	logrus.Infof("Loading config file: %s", configFile)
	viper.SetConfigFile(configFile)
	viper.SetConfigType("json")

	err := viper.ReadInConfig()
	if err != nil {
		panic("Fatal error config file")
	}

	viper.AutomaticEnv()
}

func loadGCS() {
	handler.Bucket = viper.GetString("BUCKET")
}
