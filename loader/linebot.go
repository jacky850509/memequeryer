package loader

import (
	"fmt"
	"memequeryer/api"

	"github.com/line/line-bot-sdk-go/v7/linebot"
	"github.com/spf13/viper"
)

func loadLinebot() {
	bot, err := linebot.New(viper.GetString("CHANNEL_SECRET"), viper.GetString("CHANNEL_ACCESS_TOKEN"))
	if err != nil {
		panic(fmt.Errorf("linebot error: %v", err))
	}

	api.LineBot = *bot
}
