package loader

import (
	"fmt"
	"memequeryer/model"
)

func loadES() {

	_, err := model.SendHTTPRequest("GET", "", nil)
	if err != nil {
		panic(fmt.Errorf("ES connection error: %s", err))
	}

	memeData := &model.MemeData{}
	_, err = model.SendHTTPRequest("GET", memeData.IndexName(), nil)
	if err != nil {
		panic(fmt.Errorf("%s does not in ES", memeData.IndexName()))
	}

	// client := &http.Client{}

	// req, err := http.NewRequest("GET", viper.GetString("es.url"), nil)
	// resp, err := client.Do(req)
	// if err != nil {
	// 	fmt.Println("ElasticSearch system doesn't work.")
	// 	fmt.Println(err)
	// } else {
	// 	fmt.Println("ElasticSearch loaded")
	// }
	// defer resp.Body.Close()
	// respBody, err := ioutil.ReadAll(resp.Body)
	// fmt.Println("response Status : ", resp.Status)
	// fmt.Println("response Headers : ", resp.Header)
	// fmt.Println("response Body (to string) : ", string(respBody))

	// urlList := []string{viper.GetString("es.url"), viper.GetString("es.indexName")}
	// reqIdx, err := http.NewRequest("GET", strings.Join(urlList, "/"), nil)
	// respIdx, err := client.Do(reqIdx)
	// if err != nil {
	// 	fmt.Println("ElasticSearch system doesn't exist.")
	// 	fmt.Println(err)
	// } else {
	// 	fmt.Println("ElasticSearch loaded")
	// }
	// defer resp.Body.Close()
	// respIdxBody, err := ioutil.ReadAll(respIdx.Body)
	// fmt.Println("response Status : ", respIdx.Status)
	// fmt.Println("response Headers : ", respIdx.Header)
	// fmt.Println("response Body (to string) : ", string(respIdxBody))
}
