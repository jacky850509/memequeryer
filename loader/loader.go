// Package loader
package loader

// Load - Loading config and architecture for app executing
func Load() {
	// 1. Init config
	loadConfig()
	loadGCS()
	// 2. Loading ElasticSearch
	loadES()
	// 3. Init Logger
	loggerToFile()
	// 4. Init tmp folder for upload image
	prepareTmpFolder()
	// 5. Init LineBot
	loadLinebot()
}
