package loader

import (
	"fmt"
	"memequeryer/api"
	"os"
	"path"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	apiLogger *logrus.Logger
)

func loggerToFile() {

	// 1. Get logger config
	logFilePath := viper.GetString("logger.path")
	// 2. Check file-path exist
	_, err := os.Stat(logFilePath)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(logFilePath, os.ModePerm); err != nil {
			logrus.Fatal(err)
			panic(err)
		}
	}

	// 3. APILogger Defination
	apiLogFilePath := path.Join(logFilePath, "api")
	_, err = os.Stat(apiLogFilePath)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(apiLogFilePath, os.ModePerm); err != nil {
			logrus.Fatal(err)
			panic(err)
		}
	}
	apiLogFileName := path.Join(apiLogFilePath, "api.log")
	fmt.Println("API log path: ", apiLogFileName)
	apiLogger = logrus.New()
	// lumberjack -> io.Writer
	apiLogger.SetOutput(&lumberjack.Logger{
		Filename:   apiLogFileName,
		MaxSize:    1,  // megabytes after which new file is created
		MaxBackups: 3,  // number of backups
		MaxAge:     28, //days
	})
	// logrus. InfoLevel DebugLevel
	apiLogger.SetLevel(logrus.DebugLevel)
	apiLogger.SetFormatter(&logrus.TextFormatter{})
	api.Logger = apiLogger
}
