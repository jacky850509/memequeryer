package model

import "fmt"

var (
	ES         string
	ErrCreated error = fmt.Errorf("Document create error")
	ErrSearch  error = fmt.Errorf("Document search error")
	ErrUpdate  error = fmt.Errorf("Document update error")
	ErrDelete  error = fmt.Errorf("Document delete error")
)
