package model

import (
	"encoding/json"
	"time"
)

type MemeData struct {
	ID             string    `json:"_ID,omitempty"`
	ImgDescription string    `json:"imgDescription"`
	ImgCode        string    `json:"imgCode"`
	CreateAt       time.Time `json:"createAt"`
}

type Document struct {
	Index       string          `json:"_index"`
	Type        string          `json:"_type"`
	ID          string          `json:"_id"`
	Version     int             `json:"_version"`
	Result      string          `json:"result"`
	SeqNo       int             `json:"_seq_no"`
	PrimaryTerm int             `json:"_primary_term"`
	Shard       json.RawMessage `json:"_shards"`
}

/*
type InsertDocument struct {
	Index       string          `json:"_index"`
	Type        string          `json:"_type"`
	ID          string          `json:"_id"`
	Version     int             `json:"_version"`
	Result      string          `json:"result"`
	Shard       json.RawMessage `json:"_shards"`
	SeqNo       int             `json:"_seq_no"`
	PrimaryTerm int             `json:"_primary_term"`
}
*/

/*
   query_stamt = {
       "query": {
           "bool" :{
               "must":query_condition [key1, key2]
           }
       },
       "size": limit
   }

*/

/*
type Content struct {
	QueryCondition []string `json:"must"`
}


type SubSearch struct {
	MBool Content `json:"bool"`
}*/

type Content struct {
	ImgDescription string `json:"imgDescription"`
}

type MemeMatchKV struct {
	KeyValue Content `json:"match"`
}

type MemeSearch struct {
	Query MemeMatchKV `json:"query"`
	Size  int         `json:"size"`
}

type SearchResult struct {
	Took     uint64 `json:"took"`
	TimedOut bool   `json:"timed_out"`
	Shards   struct {
		Total      int `json:"total"`
		Successful int `json:"successful"`
		Failed     int `json:"failed"`
	} `json:"_shards"`
	Hits         ResultHits      `json:"hits"`
	Aggregations json.RawMessage `json:"aggregations"`
}

type TotalKV struct {
	Value    int    `json:"value"`
	Relation string `json:"relation"`
}

// ResultHits represents the result of the search hits
type ResultHits struct {
	Total    TotalKV `json:"total"`
	MaxScore float32 `json:"max_score"`
	Hits     []struct {
		Index  string   `json:"_index"`
		Type   string   `json:"_type"`
		ID     string   `json:"_id"`
		Score  float32  `json:"_score"`
		Source MemeData `json:"_source"`
	} `json:"hits"`
}
