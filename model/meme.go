package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/spf13/viper"
)

var (
	memeIndexName = "meme"
)

func (meme *MemeData) IndexName() string {
	return memeIndexName
}

func (meme *MemeData) BeforeSave() error {
	// meme.CreateAt = time.Now()
	return nil
}

func (memeRequest *MemeData) Create() (esResp *Document, err error) {
	memeRequest.BeforeSave()
	urlList := []string{memeRequest.IndexName(), "_doc"}

	memeJson, err := json.Marshal(memeRequest)
	if err != nil {
		return nil, err
	}
	body := bytes.NewBuffer(memeJson)
	response, err := SendHTTPRequest("POST", strings.Join(urlList, "/"), body)
	if err != nil {
		return nil, err
	}
	esResp = &Document{}
	err = json.Unmarshal(response, esResp)

	if err != nil {
		return nil, err
	}
	return esResp, nil
}

func (memeData *MemeData) Delete() (esResp *Document, err error) {
	urlList := []string{memeData.IndexName(), "_doc", memeData.ID}
	// fmt.Println(strings.Join(urlList, "/"))
	response, err := SendHTTPRequest("DELETE", strings.Join(urlList, "/"), nil)
	// fmt.Println(response)
	if err != nil {
		return nil, err
	}
	esResp = &Document{}
	err = json.Unmarshal(response, esResp)
	if err != nil {
		return nil, err
	}
	// fmt.Println(esResp)
	return esResp, nil
}

func (memeSearch *MemeSearch) Search() (esResp *SearchResult, err error) {
	urlList := []string{memeIndexName, "_search"}

	memeJson, err := json.Marshal(memeSearch)
	if err != nil {
		return nil, err
	}
	// fmt.Println(string(memeJson))
	body := bytes.NewBuffer(memeJson)

	response, err := SendHTTPRequest("POST", strings.Join(urlList, "/"), body)
	if err != nil {
		return nil, err
	}

	esResp = &SearchResult{}
	err = json.Unmarshal(response, esResp)
	if err != nil {
		return nil, err
	}

	return esResp, nil
}

func SendHTTPRequest(method string, url string, body io.Reader) (response []byte, err error) {

	client := &http.Client{}
	req, err := http.NewRequest(method, strings.Join([]string{viper.GetString("ES_DSN"), url}, "/"), body)
	if err != nil {
		return nil, err
	}

	if method == "POST" || method == "PUT" {
		req.Header.Set("Content-Type", "application/json")
	}

	newReq, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer newReq.Body.Close()
	response, err = ioutil.ReadAll(newReq.Body)
	if err != nil {
		return nil, err
	}

	if newReq.StatusCode > http.StatusCreated && newReq.StatusCode < http.StatusNotFound {
		return nil, errors.New(string(response))
	}

	return response, nil
}
