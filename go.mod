module memequeryer

go 1.16

require (
	cloud.google.com/go/storage v1.18.2
	github.com/disintegration/imaging v1.6.2
	github.com/droundy/goopt v0.0.0-20170604162106-0b8effe182da
	github.com/gin-gonic/gin v1.7.4
	github.com/line/line-bot-sdk-go/v7 v7.11.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.9.0
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
