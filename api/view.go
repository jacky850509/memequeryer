package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func getUploadView(c *gin.Context) {
	c.Redirect(http.StatusMovedPermanently, viper.GetString("view.uploadLink"))
}

func configView(s string, r *gin.Engine) {
	g := r.Group(s)

	g.GET("/upload", getUploadView)
}
