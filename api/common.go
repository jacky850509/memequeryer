package api

import (
	"bytes"
	"encoding/base64"
	"image"
	"image/png"
	"memequeryer/model"
	"strings"
	"sync"

	"github.com/disintegration/imaging"
	"github.com/line/line-bot-sdk-go/v7/linebot"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type CreateMeme struct {
	ImgDescription string `json:"ImgDescription"`
	ImgCode        string `json:"ImgCode"`
}

type Meme struct {
	ImgDescription string `json:"imgDescription"`
	PreviewLink    string `json:"previewLink"`
	StandardLink   string `json:"standardLink"`
	CreateAt       string `json:"createAt"`
}

func imgResize(img image.Image) (imgPreview image.Image, imgStandard image.Image) {
	b := img.Bounds()
	imgW := b.Max.X
	imgH := b.Max.Y

	preW := viper.GetInt("img.preview.w")
	preH := int(imgH * (preW / imgW))
	imgPreview = imaging.Resize(img, preW, preH, imaging.Lanczos)

	var stdW int
	if imgW > viper.GetInt("img.standard.w") {
		stdW = viper.GetInt("img.standard.w")
	} else {
		stdW = imgW
	}

	stdH := int(imgH * (stdW / imgW))
	imgStandard = imaging.Resize(img, stdW, stdH, imaging.Lanczos)

	return imgPreview, imgStandard
}

func base64toImage(data string) (img image.Image, err error) {

	unbased, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return nil, err
	}

	r := bytes.NewReader(unbased)
	im, err := png.Decode(r)
	if err != nil {
		return nil, err
	}

	return im, nil

}

func getMeme(key string) (memes []*Meme, err error) {
	modelQuery := &model.MemeSearch{
		Query: model.MemeMatchKV{
			KeyValue: model.Content{
				ImgDescription: key,
			},
		},
		Size: 5,
	}
	searchRes, err := modelQuery.Search()
	if err != nil {
		return nil, err
	}

	// var memes []*Meme
	for _, res := range searchRes.Hits.Hits {
		meme := &Meme{
			ImgDescription: res.Source.ImgDescription,
			PreviewLink:    strings.Join([]string{viper.GetString("img.downloadLink"), viper.GetString("img.preview.tmpFolder"), res.Source.ImgCode}, "/"),
			StandardLink:   strings.Join([]string{viper.GetString("img.downloadLink"), viper.GetString("img.standard.tmpFolder"), res.Source.ImgCode}, "/"),
			CreateAt:       res.Source.CreateAt.Format("2006-01-02_150405.00"),
		}
		memes = append(memes, meme)
	}

	return memes, nil
}

func pushLineImageMessage(wg *sync.WaitGroup, to string, originalContentURL string, previewImageURL string) {
	defer wg.Done()
	_, err := LineBot.PushMessage(to, linebot.NewImageMessage(originalContentURL, previewImageURL)).Do()
	if err != nil {
		logrus.Errorf(err.Error())
	}
}

func pushLineTextMessage(wg *sync.WaitGroup, to string, text string) {
	defer wg.Done()
	_, err := LineBot.PushMessage(to, linebot.NewTextMessage(text)).Do()
	if err != nil {
		logrus.Errorf(err.Error())
	}
}
