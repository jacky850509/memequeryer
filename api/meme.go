package api

import (
	"fmt"
	"image/jpeg"
	"net/http"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"memequeryer/handler"
	"memequeryer/model"
)

func createMeme(c *gin.Context) {
	wg = &sync.WaitGroup{}

	var createMeme *CreateMeme
	err := c.BindJSON(&createMeme)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": BodyError,
		})
		return
	}

	var imgCode string
	imgCodes := strings.Split(createMeme.ImgCode, ",")
	if len(imgCodes) > 1 {
		imgCode = imgCodes[len(imgCodes)-1]
	}
	im, err := base64toImage(imgCode)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg": "圖片格式不符",
		})
		return
	}

	imgPreview, imgStandard := imgResize(im)

	now := time.Now()
	fileName := fmt.Sprintf("%s.jpg", now.Format("2006-01-02_150405.00"))

	filePreviewPath := path.Join(viper.GetString("img.preview.tmpFolder"), fileName)
	fileStandardPath := path.Join(viper.GetString("img.standard.tmpFolder"), fileName)

	filePreview, err := os.OpenFile(path.Join(viper.GetString("tmpFolder"), filePreviewPath), os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		panic("Cannot open file")
	}
	defer filePreview.Close()

	fileStandard, err := os.OpenFile(path.Join(viper.GetString("tmpFolder"), fileStandardPath), os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		panic("Cannot open file")
	}
	defer fileStandard.Close()

	jpeg.Encode(filePreview, imgPreview, &jpeg.Options{Quality: 100})
	jpeg.Encode(fileStandard, imgStandard, &jpeg.Options{Quality: 100})

	meme := &model.MemeData{
		ImgDescription: createMeme.ImgDescription,
		ImgCode:        fileName,
		CreateAt:       now,
	}

	_, err = meme.Create()
	if err != nil {
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	wg.Add(1)
	go func() {
		err = handler.UploadFileToGCS(path.Join(viper.GetString("tmpFolder"), filePreviewPath), filePreviewPath)
		if err != nil {
			logrus.Error(err)
			c.String(http.StatusExpectationFailed, err.Error())
		}
		defer wg.Done()
	}()
	wg.Add(1)
	go func() {
		err = handler.UploadFileToGCS(path.Join(viper.GetString("tmpFolder"), fileStandardPath), fileStandardPath)
		if err != nil {
			logrus.Error(err)
			c.String(http.StatusExpectationFailed, err.Error())
		}
		defer wg.Done()
	}()

	c.JSON(http.StatusCreated, gin.H{
		"msg": Created,
	})
}

func searchMeme(c *gin.Context) {
	key := c.DefaultQuery("Key", "")

	memes, err := getMeme(key)
	if err != nil {
		c.String(http.StatusInternalServerError, QueryError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": memes,
	})
}

func configMeme(s string, r *gin.Engine) {
	g := r.Group(s)
	g.POST("/", createMeme)
	g.GET("/", searchMeme)
}
