package api

import (
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/line/line-bot-sdk-go/v7/linebot"
)

var (
	LineBot linebot.Client
)

func handlerLineMessage(c *gin.Context) {
	events, err := LineBot.ParseRequest(c.Request)
	if err != nil {
		if err == linebot.ErrInvalidSignature {
			c.AbortWithStatus(400)
		} else {
			c.AbortWithStatus(500)
		}
		return
	}

	wg = &sync.WaitGroup{}

	for _, event := range events {
		wg.Add(1)
		go func(event *linebot.Event) {
			defer wg.Done()
			if event.Type == linebot.EventTypeMessage {
				switch message := event.Message.(type) {
				case *linebot.TextMessage:
					if _, err = LineBot.ReplyMessage(event.ReplyToken).Do(); err != nil {
						c.AbortWithStatus(500)
					}
					memes, err := getMeme(message.Text)
					if err != nil || len(memes) == 0 {
						wg.Add(1)
						go pushLineTextMessage(wg, event.Source.UserID, "找不到梗圖")
					}
					for _, meme := range memes {
						wg.Add(1)
						go pushLineImageMessage(wg, event.Source.UserID, meme.StandardLink, meme.PreviewLink)
					}
				default:
					if _, err = LineBot.ReplyMessage(event.ReplyToken, linebot.NewTextMessage("僅支援文字查詢")).Do(); err != nil {
						c.AbortWithStatus(500)
					}
				}
			}
		}(event)
	}

	wg.Wait()

	c.String(http.StatusOK, "OK")
}

func configLine(s string, r *gin.Engine) {
	g := r.Group(s)
	g.POST("/callback", handlerLineMessage)
}
