// Package api
package api

import (
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var (
	wg     *sync.WaitGroup
	Logger *logrus.Logger

	Created    string = "上傳成功"
	QueryError string = "查詢錯誤"
	BodyError  string = "請求資料錯誤"
)

// LoggerOut - Define logger output
func LoggerOut() gin.HandlerFunc {
	// 定義logger存儲
	return func(c *gin.Context) {
		startTime := time.Now()

		c.Next()

		endTime := time.Now()
		latencyTime := endTime.Sub(startTime)
		reqMethod := c.Request.Method
		reqUri := c.Request.RequestURI
		statusCode := c.Writer.Status()
		clientIP := c.ClientIP()

		Logger.Infof("| %3d | %13v | %15s | %s | %s |",
			statusCode,
			latencyTime,
			clientIP,
			reqMethod,
			reqUri,
		)
	}
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		// fmt.Println("I'm cors.")
		// fmt.Println(c.Writer.Header())

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

		// fmt.Println(c.Writer.Header())
		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}

		c.Next()
		// fmt.Println("I'm cors end")
		// fmt.Println(c.Writer.Header())
	}
}

// BuildRouter - Build router with gin.Engine.group to build router tree
func BuildRouter() *gin.Engine {
	// apiv1PathElements := []string{viper.GetString("router.urlPath"), viper.GetString("router.v1")}
	// apiv1Path := "/" + strings.Join(apiv1PathElements[:], "/")

	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	r.Use(CORSMiddleware())
	r.Use(LoggerOut())

	configView("/view", r)
	configMeme("/meme", r)
	configLine("/line", r)
	return r
}
