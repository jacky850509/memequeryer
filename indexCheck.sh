#!/bin/bash
es_stuts_code=$(curl -o /dev/null -s -w %{http_code} "http://localhost:9200/")
if [ ${es_stuts_code} == 200 ]; 
then
    echo "Elasticsearch services is working"
    echo "====================================="
else
    exit 1;
fi

index_stuts_code=$(curl -o /dev/null -s -w %{http_code} "http://localhost:9200/meme?pretty")

if [ $index_stuts_code == 200 ]; 
then
    echo "meme index is exist"
else
    echo "meme index is not found"
    echo "creating meme index"
    curl -X PUT "http://localhost:9200/meme?pretty" -H "Content-Type: application/json" -d "$(cat mapping.json)"

    check_stuts_code=$(curl -o /dev/null -s -w %{http_code} "http://localhost:9200/meme?pretty")

    if [ $check_stuts_code == 200 ];
    then
        echo "meme index is exist"
    fi
fi