# MemeQueryer

### System Architecture

- Ubuntu 20.04.2 LTS
- Docker 20.10.5, build 55c4c88
- docker-compose 1.25.0
- Elasticsearch 7.13.1

## Step 1: Download IK for Elasticsearch

- [Github: IK Download](https://github.com/medcl/elasticsearch-analysis-ik)

**Note:**
IK need to follow by ES version

## Step 2: Build Docker image