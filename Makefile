include .env

buildES:
	wget https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.14.2/elasticsearch-analysis-ik-7.14.2.zip -O ${ELASTICSEARCH_DIR}/elasticsearch-analysis-ik-7.14.2.zip
	docker-compose build --parallel
	mkdir ${ELASTICSEARCH_LOG} ${ELASTICSEARCH_DATA}

runES:
	docker-compose up -d

lookLogs:
	docker-compose logs

checkES:
	./indexCheck.sh

run:
	@PORT=${8080} \
		ES_DSN=${ES_DSN} \
		GIN_MODE=${GIN_MODE} \
		BUCKET=${BUCKET} \
		CHANNEL_SECRET=${CHANNEL_SECRET} \
		CHANNEL_ACCESS_TOKEN=${CHANNEL_ACCESS_TOKEN} \
		go run app/main.go