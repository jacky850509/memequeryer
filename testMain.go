package main

import (
	"encoding/json"
	"fmt"
	"memequeryer/model"
)

//"memequeryer/loader"

func main() {
	/*
		fmt.Println("Loading ElasticSearch Server (Test)")
		loader.Load()
	*/
	/*
		fmt.Println(" ElasticSearch Create Document (Test)")
		memeModel := &model.MemeData{
			ImgDescription: "wait WTF",
			ImgCode:        "waitWTF1234",
		}

		createResp, err := memeModel.Create()
		if err != nil {
			fmt.Println(err)
		}
		createJosn, err := json.Marshal(createResp)

		fmt.Println(createResp.ID)
		fmt.Println(string(createJosn))
		memeModel.ID = createResp.ID
	*/
	/*
		fmt.Println(" ElasticSearch Delete Document (Test)")

		memeModel := &model.MemeData{
			ID: "SYJAH30B88S4CZNJxlyN",
		}
		delResp, err := memeModel.Delete()
		if err != nil {
			fmt.Println(err)
		}
		delJosn, err := json.Marshal(delResp)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(string(delJosn))
	*/

	fmt.Println(" ElasticSearch Search Document (Test)")
	modelContent := model.Content{
		ImgDescription: "wait",
	}

	modelKV := &model.MemeMatchKV{
		KeyValue: modelContent,
	}

	modelQuery := &model.MemeSearch{
		Query: *modelKV,
		Size:  1,
	}
	sreachRes, err := modelQuery.Search()

	delJosn, err := json.Marshal(sreachRes)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(delJosn))

}
