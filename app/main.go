// Package main
package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/droundy/goopt"

	"memequeryer/api"
	"memequeryer/loader"
)

var (
	// BuildDate date string of when build was performed filled in by -X compile flag
	BuildDate string

	// LatestCommit date string of when build was performed filled in by -X compile flag
	LatestCommit string

	// BuildNumber date string of when build was performed filled in by -X compile flag
	BuildNumber string

	// BuiltOnIP date string of when build was performed filled in by -X compile flag
	BuiltOnIP string

	// BuiltOnOs date string of when build was performed filled in by -X compile flag
	BuiltOnOs string

	// RuntimeVer date string of when build was performed filled in by -X compile flag
	RuntimeVer string

	// OsSignal signal used to shutdown
	OsSignal chan os.Signal
)

// GinServer - Build the Gin RestfulAPI Server
// return - gin.Router
// error - gin.Router init error
func GinServer() (err error) {

	router := api.BuildRouter()

	router.Run()
	if err != nil {
		log.Fatalf("Error starting server, the error is '%v'", err)
		panic("Gin init error.")
	}

	return
}

func main() {
	// Load System config and architecture
	loader.Load()

	OsSignal = make(chan os.Signal, 1)

	// Define version information
	goopt.Version = fmt.Sprintf(
		`Application build information
  Build date      : %s
  Runtime version : %s
  Built on OS     : %s
  Build number    : %s
  Git commit      : %s
`, BuildDate, RuntimeVer, BuiltOnOs, BuildNumber, LatestCommit)
	goopt.Parse(nil)

	go GinServer()
	LoopForever()
}

// LoopForever on signal processing
func LoopForever() {
	fmt.Printf("Entering infinite loop\n")

	signal.Notify(OsSignal, syscall.SIGINT, syscall.SIGTERM, syscall.SIGUSR1)
	_ = <-OsSignal

	fmt.Printf("Exiting infinite loop received OsSignal\n")

}
